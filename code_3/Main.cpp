#include <iostream>
using namespace std;

#include "Lista.h"

//Funcion que sirve para limpiar la pantalla, no influye en nada
//del funcionamiento del codigo, es mas algo estetico.
void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__Apple__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

//Funcion que nos pemite crear una lista y acceder al menu.
void crea_lista(Lista *lista){
	int opc, num_temp;

		//Ciclo principal
		while(opc != 0){
			cout << "[1] Agregar numero" << endl <<
					"[2] Rellenar lista" << endl <<
					"[3] Salir" << endl <<
					"Opcion: ";
			cin >> opc;
			
			//Ingresa numeros.
			if(opc == 1){
				cout << "Ingrese el numero: ";
				cin >> num_temp;
				cout << endl;
				lista->agregar(num_temp);
				lista->imprimir();
			}
			
			//Rellenar la lista.
			else if (opc == 2){
				lista->rellenar();
			}
			//Salir del programa.
			else if (opc == 3){
				opc = 0;
			}
			
			else{
				cout << "Opcion invalida..." << endl;
			}
		}
}


//FUNCION MAIN
int main (void) {
	
	Lista *lista = new Lista();
	crea_lista(lista);
	clear();
	
    return 0;
}
