#ifndef LISTA_H
#define LISTA_H

#include <iostream>

using namespace std;

//ESTRUCTURA DEL NODO
typedef struct _Nodo {
    int numero;
    struct _Nodo *sig;
} Nodo;

class Lista {
	private:
		Nodo *principal = NULL;
		
    public:
        //CONSTRUCTOR POR DEFECTO
        Lista();
        
        //Se crea un nodo que recibe un objeto tipo numero.
        void agregar(int num);
        
        //Imprime la lista actual.
        void imprimir();
        
        //Rellena la lista actual.
        void rellenar();
};
#endif
