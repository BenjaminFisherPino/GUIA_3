#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista() {}

void Lista::agregar(int numero) {
    Nodo *q;
    int resultado;

    //Se crea un nodo.
    q = new Nodo;

    //Se asigna el nodo al objeto numero.
    q->numero = numero;
    
    //Se inicializa como NULL por defecto.
    q->sig = NULL;

    /*Si el es primer nodo de la lista, lo deja
    como raíz y como último nodo.*/
    if (this->principal == NULL) { 
		this->principal = q;

    /*De lo contrario, apunta el actual último nodo 
    al nuevo y deja el nuevo como el último de la lista. */
    }
    
    else {
		Nodo *s, *r;
		s = this->principal;
		
		while ((s != NULL) && (s->numero < numero)){
			r = s;
			s = s->sig;
		}
		
		if (s == this->principal){
			this->principal = q;
			this->principal->sig = s;
		}
		
		else{
			r->sig = q;
			q->sig = s;
		}
    }
}

void Lista::imprimir() {
	
    //Utiliza variable temporal para recorrer la lista.
    Nodo *q = this->principal;
    
    //Contador
	int i = 1;
	
    //La recorre mientras sea distinto de NULL (no hay más nodos).
    while (q != NULL) {
        cout <<  i << ") " << q->numero << endl;
        i++;
        q = q->sig;
    }
    cout << endl;
}

//Funcion que nos permirte rellenar la lista
void Lista::rellenar() {
	
	Nodo *s, *r;
	int n, resultado;
	s = this->principal;
	
	while ((s != NULL)){
			r = s;
			s = s->sig;
			
			//Mientras s->liga no sea NULL, se podra
			//acceder a este ciclo
			if (s != NULL){
				int a = r->numero;
				int b = s->numero;
				resultado = b - a;//Esto siempre funciona porque
									  //la lista esta ordenada
				
				//Si hay una diferencia mayor a uno significa que
				//hay vacios en nuestra lista actual. 
				if (resultado > 1){
					n = 1;
					
					for (int i = 0; i < resultado - 1; i++){
						agregar(a+n);
						n++;
					}
				}
			}
		}
		cout << endl;
}
