# Archivo README.md

**Versión 1.0**

Archivo README.md para guía número 3 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 22/09/2021

---
## Resúmen del programa

Estos programas fueron desarrollados por Benjamín Fisher para almacenar ordenada en listas dinamicas de nodos.

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0 y la version 4.2.1 de make. En caso de no tener el compilador de g++ o make y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable de los programas.

---
## Como instalarlo

Para descargar los archivos debe dirigirse al repositorio alocado en la siguiente URL.

Link: https://gitlab.com/BenjaminFisherPino/GUIA_3.git

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe escoger el programa que desea utilizar, por ejemplo si quiere utilizar el codigo 1 usted debe ingresar a la carpeta 1 y escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto"

El mismo proceso se debe seguir para poder utilizar los otros dos codigos.

En caso de tener problemas asegurece de tener la version 9.3.0 de g++ y la version 4.2.1 de make

Puede revisar con "g++ --version" y "make --version" respectivamente.

Si siguio todos los pasos debiese estar listo para ejecutar los programas!

---
## Funcionamiento del programa

Programa 1: Este programa nos permite almacenar números en una lista dinamica de forma ordenada y creciente. El codigo se estructura en base a una funcion main, la cual define el orden de compilacion del programa y nos sirve como intercomunicador entre el codigo y el usuario. El programa consta de dos opciones, la primera nos permite agregar un nodo a la lista y asignarle un valor numerico ingresado por teclado, la segunda opcion nos permite finalizar con la ejecucion del programa.

Programa 2: Este programa nos permite almacenar numeros ordenados de forma creciente en una lista A y en una lista B y tras finalizada la ejecucion del programa, mezclar los numeros de ambas listas en una lista C. El codigo es muy similar al anterior, de hecho se reutiliza el codigo de este, lo unico que difiere es la implementacion de una nueva funcion que nos permite mezclar ambas listas. Las opciones siguen siendo las mismas, solo que ahora al presionar la opcion 2 (salir del programa) se accede a la lista B, y si se vuelve a presionar esta opcion, se procede a mezclar las listas y finalizar la ejecucion del programa.

Programa 3: Este programa, al igual que el primero, nos sirve para almacenar numeros en una lista ordenada creciente, solo que ahora nos dan la opcion de rellenar la lista para que no hayan vacios entre los numeros. Se reutilizo el codigo del primer programa y se añadio una funcion llamada "rellenar", la cual nos permite llenar los vacios entre dos nodos de nustra lista. Las opciones cambian un poco, la opcion 1 nos permite agregar un numero, la opcion 2 nos permite llenar la rellenar la lista y la tercera opcion nos permite finalizar la ejecucion del programa.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca
