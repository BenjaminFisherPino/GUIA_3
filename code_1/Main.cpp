#include <iostream>
using namespace std;

#include "Lista.h"

//Funcion que sirve para limpiar la pantalla, no influye en nada
//del funcionamiento del codigo, es mas algo estetico.
void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__Apple__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

//FUNCION MAIN
int main (void) {
	
	Lista *lista = new Lista();
	int resp, opc, num_temp;
	
	resp = 1;
	
	cout << "Bienvenido!" << endl;
	
	//CICLO PRINCIPAL
	while(resp != 0){
		
		cout << "[1] Ingresar un numero " << endl;
		cout << "[2] Salir del programa " << endl;
		cout << "Opcion: ";
		cin >> opc;
		
		clear();
		
		//Agrega un numero a la lista.
		if (opc == 1){
			cout << "Ingrese el numero: ";
			cin >> num_temp;
			cout << endl;
			lista->agregar(num_temp);
			lista->imprimir();
		}
		
		
		//Opcion para salir del ciclo principal.
		else if (opc == 2){
			
			//Libera la memoria utilizada por el programa.
			cout << "Se libero la memoria utilizada..." << endl;
			delete lista;
			
			cout << "Hasta luego!" << endl;
			resp = 0;
		}
		
		//Opcion invalida...
		else{
			cout << "Opcion invalida..." << endl << endl;
		}
	}
    return 0;
}
