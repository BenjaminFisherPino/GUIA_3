#include <iostream>
using namespace std;

#include "Lista.h"

//Funcion que sirve para limpiar la pantalla, no influye en nada
//del funcionamiento del codigo, es mas algo estetico.
void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__Apple__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

//Funcion que nos permite crear las listas 1 y 2
void crea_lista(Lista *lista){
	int opc, num_temp;
		while(opc != 0){
		cout << "[1] Agregar numero" << endl <<
	               "[2] Salir de la lista" << endl <<
	               "Opcion: ";
		cin >> opc;
		
		if(opc == 1){
			cout << "Ingrese el numero: ";
			cin >> num_temp;
			cout << endl;
			lista->agregar(num_temp);
			lista->imprimir();
		}
		
		//Salir de la lista. 
		else if (opc == 2){
			opc = 0;
		}
		
		else{
			cout << "Opcion invalida..." << endl;
		}
	}
}

//FUNCION MAIN
int main (void) {
	
	Lista *lista1 = new Lista();
	Lista *lista2 = new Lista();
	Lista *lista3 = new Lista();
	
	cout << "Bienvenido!" << endl << endl;
	
	crea_lista(lista1);
	clear();
	crea_lista(lista2);
	clear();
	
	//Aqui se mezclan la Lista 1 y 2 formando la 3
	Nodo *q;
	q = lista1->principal;
	while(q != NULL){
		lista3->agregar(q->numero);
		q = q->sig;
	}
	
	q = lista2->principal;
	while(q != NULL){
		lista3->agregar(q->numero);
		q = q->sig;
	}
	
	lista3->imprimir();
	
	//Recuperando memoria utilizada
	delete lista1, lista2, lista3;
	
    return 0;
}
